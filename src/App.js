import './App.css';
import Fragment from './components/Fragment';

function App() {
  return (
    <div>
      <h1>Example of Fragment</h1>
      <Fragment />
    </div>
  );
}

export default App;
