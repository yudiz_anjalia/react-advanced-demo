import React, { useState } from 'react';
 
function ReactDecrement() {
  const [count, setCount] = useState(0);
 
  const decrement = () => {
    setCount(count => count - 1);
  };
 
  return (
    <button className='button' onClick={decrement}>Clicked {count} times</button>
  );
}

export default ReactDecrement;