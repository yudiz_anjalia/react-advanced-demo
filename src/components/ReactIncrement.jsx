import React, { useState } from 'react';
 
function ReactIncrement() {
  const [count, setCount] = useState(0);
 
  const increment = () => {
    setCount(count => count + 1);
  };
 
  return (
    <button className='button' onClick={increment}>Clicked {count} times</button>
  );
}

export default ReactIncrement;