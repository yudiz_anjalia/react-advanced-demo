import React from 'react';
import ReactDecrement from './ReactDecrement';
import ReactIncrement from './ReactIncrement';

function Fragment() {

    return (
        <React.Fragment>
            <ReactIncrement />
            <ReactDecrement />
        </React.Fragment>
    )
}

export default Fragment;